#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

for BULB in ${SERIES_STUDY[*]}; do
  set_colour ${BULB} ${COLOUR_COLD}
  set_brightness ${BULB} ${LAMP_MIN}
  set_slow_brightness ${BULB} ${LAMP_MAX} 900
done
