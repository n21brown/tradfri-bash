#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

for BULB in ${SERIES_MASTER_BEDROOM[*]}; do
  set_colour ${BULB} ${COLOUR_WARM}
  set_polite_brightness ${BULB} 100
done
