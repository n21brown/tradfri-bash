#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

if [ -z "${1}" ]; then
  echo "ERROR: Need a device ID"
  exit 1
fi

DEVICE=$(name_to_num ${1})

get_state ${DEVICE}
