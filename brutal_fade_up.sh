#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

if [ -z "${1}" ]; then
  echo "ERROR: Need a device ID"
  exit 1
fi

DEVICE=$(name_to_num ${1})

BRIGHT=0
set_bright ${BRIGHT}

while [ ${BRIGHT} -lt 256 ]; do
  BRIGHT=$((BRIGHT+1))
  set_bright ${BRIGHT}
  sleep 1
done
