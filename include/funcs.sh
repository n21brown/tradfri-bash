#!/bin/bash

COAP="/usr/local/bin/coap-client"
JQ="/usr/bin/jq"
TIMEOUT=30
DELAY=1

# check basic config has taken place
if [ -z "${GATEWAY_IP}" ]; then
  echo "ERROR: Set the GATEWAY_IP in include/config.sh before use"
  exit 1
fi

if [ -z "${IDENT}" ]; then
  echo "ERROR: Run set_identity.sh before use"
  exit 1
fi

if [ -z "${IDENT_KEY}" ]; then
  echo "ERROR: Run set_identity.sh before use"
  exit 1
fi

# Groups
function set_group_brightness {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" -e "{ \"5851\" : ${2} }"
  sleep ${DELAY}
}

function set_slow_group_brightness {
  SECS=${3}
  DURATION=$((SECS*10))
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" -e "{ \"5712\" : ${DURATION}, \"5851\" : ${2} }"
  sleep ${DELAY}
}

function get_group_brightness {
  ${COAP} -m get -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" 2>&1 | sed 1d | ${JQ} '."5851"'
  sleep ${DELAY}
}

function set_group_state {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" -e "{ \"5850\" : ${2} }"
  sleep ${DELAY}
}

function get_group_state {
  ${COAP} -m get -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" 2>&1 | sed 1d | ${JQ} '."5850"'
  sleep ${DELAY}
}

function set_group_on {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" -e "{ \"5850\" : 1 }"
  sleep ${DELAY}
}

function set_group_off {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" -e "{ \"5850\" : 0 }"
  sleep ${DELAY}
}

# this one doesn't work as a direct colour set, but using presents might
function set_group_colour {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15004/${1}" -e "{ \"9039\" : ${2} }"
  sleep ${DELAY}
}


# Lamps
function set_brightness {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5851\" : ${2} }] }"
  sleep ${DELAY}
}

function set_slow_brightness {
  SECS=${3}
  DURATION=$((SECS*10))
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5712\" : ${DURATION}, \"5851\" : ${2} }] }"
  sleep ${DELAY}
}

function get_brightness {
  ${COAP} -m get -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" 2>&1 | sed 1d | ${JQ} '."3311" [0] ."5851"'
  sleep ${DELAY}
}

function set_state {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5850\" : ${2} }] }"
  sleep ${DELAY}
}

function get_state {
  ${COAP} -m get -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" 2>&1 | sed 1d | ${JQ} '."3311" [0] ."5850"'
  sleep ${DELAY}
}

function set_colour {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5706\" : \"${2}\" }] }"
  sleep ${DELAY}
}

function set_on {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5850\" : 1 }] }"
  sleep ${DELAY}
}

function set_off {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5850\" : 0 }] }"
  sleep ${DELAY}
}

# this doesn't work, and the order of the 5850/5851 pair don't alter that :(
function set_silent_bright {
  ${COAP} -m put -u "${IDENT}" -k "${IDENT_KEY}" -B ${TIMEOUT} "coaps://${GATEWAY_IP}:5684/15001/${1}" -e "{ \"3311\" : [{ \"5850\" : 0, \"5851\" : ${2} }] }"
  sleep ${DELAY}
}

# Helpers
function name_to_num {
  if [[ ${1} =~ ^[0-9]+$ ]] ; then
    echo "${1}"
  else
    echo ${!1}
  fi
}

function set_polite_brightness {
  local THIS_STATE THIS_BRIGHT

  THIS_STATE=$(get_state ${1})
  if [ "null" == "${THIS_STATE}" ]; then
    THIS_STATE=0
  fi
  if [ ${THIS_STATE} -eq 0 ]; then
    set_brightness ${1} ${2}
    set_state ${1} ${THIS_STATE}
  else
    THIS_BRIGHT=$(get_brightness ${1})
    if [ "null" == "${THIS_BRIGHT}" ]; then
      THIS_BRIGHT=256
    fi
    if [ ${2} -lt ${THIS_BRIGHT} ]; then
      set_slow_brightness ${1} ${2} 10
    fi
  fi
}

function set_polite_group_brightness {
  local THIS_STATE THIS_BRIGHT

  THIS_STATE=$(get_group_state ${1})
  if [ "null" == "${THIS_STATE}" ]; then
    THIS_STATE=0
  fi
  if [ ${THIS_STATE} -eq 0 ]; then
    set_group_brightness ${1} ${2}
    set_group_state ${1} ${THIS_STATE}
  else
    THIS_BRIGHT=$(get_group_brightness ${1})
    if [ "null" == "${THIS_BRIGHT}" ]; then
      THIS_BRIGHT=256
    fi
    if [ ${2} -lt ${THIS_BRIGHT} ]; then
      set_slow_group_brightness ${1} ${2} 10
    fi
  fi
}
