#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

if [ -z "${1}" ]; then
  echo "ERROR: Need a device ID"
  exit 1
fi

DEVICE=$(name_to_num ${1})

if [ -z "${2}" ]; then
  echo "ERROR: Need a colour (cold|sun|mid|warm)"
  exit 1
fi

case "${2}" in
  cold) RGB_VALUE=${COLOUR_COLD}
  ;;
  sun) RGB_VALUE=${COLOUR_SUN}
  ;;
  mid) RGB_VALUE=${COLOUR_MID}
  ;;
  warm) RGB_VALUE=${COLOUR_WARM}
  ;;
esac

if [ -z "${RGB_VALUE}" ]; then
  echo "ERROR: Can't find a matching colour value"
  exit 1
fi

set_colour ${DEVICE} ${RGB_VALUE}
