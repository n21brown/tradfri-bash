#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

coap-client -v 0 -m get -u "${IDENT}" -k "${IDENT_KEY}" "coaps://${GATEWAY_IP}:5684/139121"
