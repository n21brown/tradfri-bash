#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh

if [ -z "${iGATEWAY_IP}" ]; then
  echo "ERROR: Set the GATEWAY_IP in include/config.sh before use"
  exit 1
fi

if [ -z "${IDENT}" ]; then
  echo "ERROR: Set the IDENT value in include/config.sh before use"
  exit 1
fi

if [ ! -z "${IDENT_KEY}" ]; then
  echo "ERROR: The IDENT_KEY in include/config.sh is already configured"
  exit 1
fi

if [ -z "${1}" ]; then
  echo "ERROR: Missing Gateway Key (found the base of unit)"
  exit 1
fi

coap-client -m post -u "Client_identity" -k "${1}" -e "{\"9090\":\"${IDENT}\"}" "coaps://${GATEWAY_IP}:5684/15011/9063"

echo " "
echo "NOTE: Copy the string return above into IDENT_KEY in include/config.sh"
