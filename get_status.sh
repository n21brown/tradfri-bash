#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

DEVICE=""

if [ ! -z "${1}" ]; then
  DEVICE=$(name_to_num ${1})
fi

coap-client -v 0 -m get -u "${IDENT}" -k "${IDENT_KEY}" "coaps://${GATEWAY_IP}:5684/15001/${DEVICE}" 2>&1 | sed 1d | jq -S
