# About tradfri-bash
A simple, pared back easy to use set of `bash` scipts with minimal dependancies to allow house automation of Ikea's Tradfri lights via their gateway unit (v1.2.42 - Homekit enabled version).

# Requirements
Makes heavy use of the `coap-client` cli tool, built with DTLS as per the instructions found in many places online (ie: http://kuehnast.com/s9y/archives/657-Manage-your-IKEA-TRDFRI-smart-lighting-system-with-Linux.html):

```
sudo apt-get install libtool autoconf automake buid-essential
git clone --recursive https://github.com/obgm/libcoap.git
cd libcoap
git checkout dtls
git submodule update --init --recursive
./autogen.sh
./configure --disable-documentation --disable-shared
make
sudo make install
```

The JQ tool is used to help prettify the JSON return values, and can be installed on Debian-like O/S with:

```
apt-get install jq
```

# Aims and Uses
There are many, many Tradfri tools out there, including whole Home Automation systems, all lovingly crafted in impenetrable python/pip edifices which are just way more complex than things need to be.  This is a stripped back, bare bones convienece wrapper around `coap-client` which does _some_ error checking but pretty much leaves you alone to break things in many more interesting ways.

It deliberately has few dependancies and requires almost no addtional resources of note over that of a `bash` shell, and fits nicely into `crontab` setups.  Allows quick starts and many hours of tinkering, but in now way replaces a dully fledged automation system.

# Notes
- Only tested with the following Tradfri hardware:
  - GU10 400 lumen white spectrum
  - E17 400 lumen white spectrum
  - E27 1000 lumen warm white
  - E27 980 lumen white spectrum
  - E27 950 lumen white spectrum
  - Tradfri Home Gateway
- White spectrum bulbs can have their colour temperature set when on or off
- Bulbs cannot have their brightness set for 'next turn on' - the workaround is to turn on for 1 second and set the brightness, which is not desirable for inhabited rooms overnight
- All scrpts will accept the numeric ID for a lamp, and can optionally be given an ASCII name which is a `bash` variable configured in the `include/names.sh` file.  Setting up this file is 100% manual.
- Group suppport is primitive, and less relaible than direct bulb addressing for me
- Preset (Mood) identitities appear to vary between rooms, but bulk queries that are too frequent may upset the gateway
- Expects Homekit-enabled Gateway firmware which supports the custom identity authentication process (v1.2.42)
- Min and Max brightness values are 1 and 254 which is a change from earlier firmware versions: using 255 will result in an error in v1.2.42

# Getting Started
First steps:
- Rename `include/config_example.sh` to `include/config.sh`
- Edit `include/config.sh` and enter the LAN IP address of the Tradfri Gateway
- Run `set_identity.sh` with the key on the bottom of the Gateway as an argument to the script
- Copy the identity return string out of the JSON block displayed
- Edit `include/config.sh` to set `IDENT_KEY` to the value from the JSON return
- Rename `include/names_example.sh` to `include/names.sh`

The system is now configured and ready to use for basic queries - the first step will be to use the `get_status.sh` script alone to get an initial list of items on your network, and then with an argument to deep-dive into each of the returned values to discover the names associated with each ID.

Edit the `include/names.sh` file to suit your own setup.  Note that the `GROUP_` entries map to the Tradfri Group concept, and that `SERIES_` are simple lists which can be expanded via appending `[*]` to the name in `bash` and results in individual lamp transactions.  Groups can accept Presets (Moods) but the values may vary for each Group.  Groups cannot accept colour temperature settings, but individual bulbs can.

# Files

### `set_identity.sh`
Use to set up the system to have a unique ID and KEY just for this script.  See the Getting Started section for more details.

### `brutal_fade_up.sh`
Test script showing how to brute-force a fade with multiple calls to set the brightness of a lamp.  Exceeding unrefined: use the fade-up call instead.

### `fade_colour.sh`
Cycle through various colour hex codes for a specificied lamp ID.

### `get_brightness.sh`
Return a value between 0 and 254 for the current brightness of any specified lamp, with 0 setting the lamp to off.  Helpful shortcuts are created in `include/names.sh`.

### `get_endpoints.sh`
Request all of the endpoints as described in the COAP specifictions.

### `get_frag.sh`
A simple helper to hide the messy `coap-client` call and allow the useful and changing part of the COAP request to be submitted as an argument.  Useful to looking at values returned from `get_endpoints.sh` in more detail.

### `get_state.sh`
Return the on/off state of a specified lamp (0=off, 1=on).  Useful to wrap calls to setting the brightness

### `get_status.sh`
Run without arguments to get a list of all devices registered with the home gateway, or provide a numberic ID/name to get detailed information on that item.  Look in the `include/names.sh` file for the mapping of ID to name.

### `mqtt_tradfri.sh`
An example script to listen for a Sonoff-Tasmota (https://github.com/arendst/Sonoff-Tasmota) MQTT endpoint, and turn a Tradfri bulb on or off in step with the Sonoff controlled device, with an ON event having a 10 second fade up, and an OFF having a 15 minute fade to off.

### `set_brightness.sh`
Run with an ID/name and brightness value (0 to 255, with 0=off) to set that lamp to the desired level

### `set_slow_brightness.sh`
Run with an ID/name, brightness value (0 to 255, with 0=off) and duratiokn in seconds to set that lamp to the desired level after the specified duration.  Can be used for a faster wakeup alarm than the app (ie: 900 seconds for a 15 minute soft start, or 10 seconds for a subtle change in level at various points of the evening)

### `set_colour.sh`
Run wth a lamp ID/name and a colour name as specified in `include/names.sh` to set that lamp to the desired colour.

### `set_off.sh`
Run with an ID/name to turn that lamp off.

### `set_on.sh`
Run with an ID/name to turn that lamp on.

### `set_state.sh`
Run with an ID/name and a state (0=off, 1=on) to set the lamp to that state.  Useful when storing the values of `get_state.sh` in a varable as part of a brightness setting exercise.  For example:

- Read state
- Set desired brightness
- Set state

Will ensure that if the lamps are off then (apart from a 1 second on to set the value) the next time they turn on they will be at the desired brightness, and if they are on then they remain on and just change brightness immediately.

### `set_group_brightness.sh`
Expects a `GROUP_` name from `include/names.sh` as the first argument, and a vlaue between 1 and 254 for the second.

### `set_group_colour.sh`
If the appropriate `GROUP_` and `PRESET_` values are defined in `include/names.sh` then use a single data curst to control changes to a predefined Mood.  Note that these values may change by room - Group use has not been fully investogated.

### `set_group_off.sh`
Takes a `GROUP_` name/ID as an argument and turns that collection of bulbs off.

### `set_group_on.sh`
Takes a `GROUP_` name/ID as an argument and turns that collection of bulbs on.

### `set_group_state.sh`
Set a `GROUP_` of bulbs to on or off depending on the second argument to the script (0 == OFF, 1 == ON)

### `get_group_brightness.sh`
Takes a `GROUP_` name/ID as an argument and returns the brightness of the group (0 to 254).

### `get_group_state.sh`
Takes a `GROUP_` name/ID as an argument and returns the state of that collection of bulbs (0 == OFF, 1 == ON)

### `get_group_status.sh`
Takes a `GROUP_` name/ID as an argument and shows the JSON data for that group.

### `get_presets.sh`
List Presets - this is a small fragment which may not work.  Check for the correct Preset value for your system.

### ` timer/daytime.sh`
Run at 06:00 every morning to set the colour to the coldest 'wakeup' value and ensure hall way lighting comes on at full power.

### ` timer/wakeup.sh`
Run at 06:35 every weekday to give a 15 minute fade-up.

### ` timer/evening.sh`
Run at 18:00 every day to turn on table/security lamps and switch to a warmer colour temperature everywhere.

### ` timer/night.sh`
Run at 20:30 every day to move to the warmest colour tempertaure and dim hall lighting.

### ` timer/prebedtime.sh`
Run at 21:45 every day to dim the bedroom lighting before it's likely to be used.

### ` timer/overnight.sh`
Run at 22:30 weekdays, and 23:00 on weekends to turn off the table/security lamps and dim hallways lighting to the lowest level.

### ` include/config_example.sh`
Rename this file to `include/config.sh` before use.  Change the IP address to that of your Home Gateway unit, and the security key to the value on the label on the base of the unit.

### ` include/names_example.sh`
Rename this file to `include/names.sh` before use.  Leave the colour temperature names in place if you wish, but create individual name to number mappings and your own Groups as desired.

### `include/funcs.sh`
Where the `coap-client` work stakes place, so read and reuse this for your own needs if you just want the `coap://` URI portions.  Note that each network call comes with an enforced delay to help prevent hammering the Gateway with traffic - this is by design as there is no error or success checking done for any of the `coap-client` calls.
