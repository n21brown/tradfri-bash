#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

MQTT=/usr/bin/mosquitto_sub
DEVICE=$(name_to_num LIVING_ROOM_LAMP)
MQTT_IP=""

if [ -z "${MQTT_IP}" ]; then
  echo "ERROR: Need an MQTT broker IP"
  exit 1
fi

if [ -z "${DEVICE}" ]; then
  echo "ERROR: Need a device ID"
  exit 1
fi

while read MSG
do
  echo "Heard: '${MSG}'"
  if [ "ON" == "${MSG}" ]; then
    set_slow_brightness ${DEVICE} ${LAMP_MAX} 10
  fi
  if [ "OFF" == "${MSG}" ]; then
    set_slow_brightness ${DEVICE} ${LAMP_OFF} 900
  fi
done < <(${MQTT} -h ${MQTT_IP}  -t 'stat/sonoff/POWER')
