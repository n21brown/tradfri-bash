#!/bin/bash

INC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/include"

source ${INC}/config.sh
source ${INC}/names.sh
source ${INC}/funcs.sh

if [ -z "${1}" ]; then
  echo "ERROR: Need a device ID"
  exit 1
fi

DEVICE=$(name_to_num ${1})

if [ -z "${2}" ]; then
  echo "ERROR: Need a brightness"
  exit 1
fi

if [ -z "${3}" ]; then
  echo "ERROR: Need a duration (in seconds)"
  exit 1
fi

set_slow_brightness ${DEVICE} ${2} ${3}
